using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets._2D;

public class RobotBoy : MonoBehaviour
{
    public Transform respawnPoint;
    public AudioClip winSound;
    public AudioClip deathSound;
    
    private Platformer2DUserControl userControl;
    private AudioSource audioSource;

    private bool once;

    void Start() {
        audioSource = GetComponent<AudioSource>();
        userControl = GetComponent<Platformer2DUserControl>();
        once = false;
    }

    void OnTriggerEnter2D(Collider2D collision) {
        // Character has multiple colliders,
        // but only want one trigger
        if (once) {
            return;
        }

        if (collision.tag == "Flag") {
            once = true;
            // Play sound
            audioSource.PlayOneShot(winSound);
            // Disable movement
            userControl.enabled = false;
            StartCoroutine(Respawn(6f));
        }
        else if (collision.tag == "KillBox") {
            once = true;
            // Play death sound
            audioSource.PlayOneShot(deathSound);
            // Respawn
            StartCoroutine(Respawn(2.9f));
        }
    }

    IEnumerator Respawn(float delay) {


        // Wait for sound
        yield return new WaitForSeconds(delay);

        // Move back to start
        transform.position = respawnPoint.position;

        // reset once
        once = false;

        // Enable movement (in case turned off)
        userControl.enabled = true;
    }
}
