using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingVerticalPlatform : MonoBehaviour
{
    public float speed;
    public float epsilon;
    private float initialY;
    private float direction = 1f;

    void Start() {
        initialY = transform.position.y;
    }

    void Update() {
        transform.position += new Vector3(0, speed * Time.deltaTime * direction);
        if (Mathf.Abs(transform.position.y - initialY) >= epsilon) {
            direction *= -1f;
        }
    }
}
