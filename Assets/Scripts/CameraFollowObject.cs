using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowObject : MonoBehaviour
{
    public Transform followObject;

    void Update()
    {
        transform.position = new Vector3(followObject.position.x, transform.position.y, transform.position.z);
    }
}
